\documentclass[11pt]{article}

\usepackage{fullpage}

\begin{document}
\title{ARM Group 2: Binary Counter}
\author{Adam Wiles, Chase Hellemans, Jamshid Barry, Oliver Robinson}

\maketitle

\section{Introduction}

For our extension we chose to create a 3 bit binary counter with LED's using the
Raspberry Pi. So by using the tools we created in the first 2 parts of the project,
we were able to construct the ARM assembly language program to achieve this. Along
the way we also implemented extra functionality for our assembler as well as later
extending our extension.

As a group we feel as though we have been able to learn and develop both our C 
programming and group working skills throughout the duration of the project. This has
benefitted us all and allowed us to make quick and efficient progress.

\section{Description of Extension}

Our initial extension was to create a binary counter using LED's, this seemed to be an
attainable goal in the limited time left, although we easily completed the extension
within time and thus decided to further the extension.

In the course of doing so, we also added support for comments in our assembler as we
thought this would be extremely useful for writing long assembly language code. This
helped to maintain readability within our code, allow for quicker debugging as well
as later re-use of the code with little effort to find the required functionality. We
implemented support for both new line comments as well as same line comments
(comments on the same line as assembly code).

As a further challenge and a bit of fun, we chose to make the LED's light up in a
pattern of our choosing, before then having the idea to create a program which allows
users to specify a particular pattern in the sequential order they wish the lights to
follow. The idea being that this program produces both the ARM assembly code and the
required kernel img file.

The binary counter, if scaled up, could be used not only as a timer but also a
7-segment display to show the value of the digit. The idea of the LED pattern maker
is that it allows the users to quickly compose a sequence of LED flashes which has
the potential to be used for arcade-esque displays. 

\section{High-level details of design}

We first created a main loop for the program which created the basic outline of the
binary counter. To reduce the duplication of code and increase efficiency we made use
of labels to imitate function calls. We are aware that by implementing a stack,
function calls themselves can be used on the ARM architecture, yet in the interest of
time we chose to make use of labels.

\subsection{Binary Counter Walkthrough}

First, we set all the desired pins as output pins by writing 001 to appropriate memory
locations, as this would allow us to use the pins to light up the connected LEDs.
We would then begin our main loop, effectively a switch statement. Our code
consisted of two inner loops, the first is the binary counter.
We made a series of branch jumps to the label corresponding to the current digit and used a counter to keep track of the current digit. We then created a switch-like statement using a series of comparisons, to select the correct branch to jump to. The second loop is then used to display our
pattern, using the same techniques we used for the counter. By using this structure, we
created a systematic method for constructing various light sequences, where we only
had to write the code for switching certain light combinations on once, and then
repeatedly branch to it when required. After each of these branches, we would use a
very long loop to cause a delay (Counting down from 0x1D0000), and leave the lights on or off for a certain amount of time. Lastly, we clear each pin by writing  a one shifted by the required amount to the appropriate memory location. 

\subsection{Commenting}

As well as this, we also added support for commenting the ARM assembly code, improving the readability, ease of debugging and re-usability of the code. This was a simple and effective addition to our assembler. We were able to identify comments by checking for the presence of a semi-colon at the begininning of a line (if the comment is on it's own line), or even if the comment is
on the same line as existing assembly code. So long as the comment is prefixed with a
semi-colon, comments will be ignored at assembly time. We did this by tokenising each
line, using a semi-colon as the break character, and ignoring anything that comes after
it. 

\section{Testing}

Once we had completed the various parts of our code, we spent a lot of time testing
each part to ensure it worked as intended. For most of this we used the provided test
suite, which allowed us to see if our emulator and assembler programs were working
properly. However, one interesting issue we had with this was that later into the
project, we would run the test suite and receive a different number of passes and
fails each time, despite not changing the code. We realised this was due to a number
of memory leaks in our code, which would only sometimes cause the test cases to fail.
We made use of tools such as Valgrind to help find and fix the leaks. These were very
helpful, and saved us a lot of time debugging, as we were not used to working with
a low level language like C, and it was a problem we did not usually have to deal with
in languages such as Java.

Another interesting case we found when it came to testing our code was the fact that
when some members of the group ran the emulator and assemble programs on their own
computers, they got errors which did not affect the programs on other machines,
such as the lab computers. We put this down to more memory leaks, and the way different
computers allocated the memory for C. Once again, we were able to fix this by
thinking more carefully about the way we allocated memory.

Dealing with these problems taught us how important it is to keep track of memory in
languages such as C, as it can make a huge difference to the performance/reliability
of the program.

As for testing our extension, we began by using the emulator to test our program before
placing the kernel image on the Pi itself and running it. Through the additional
features added to our emulator in part three, we were able to use it to tell whether
the correct row of pins was accessed and when a pin was turned off or on.
This allowed us to quickly and easily get a rough idea of what to expect, and
whether our program was accessing the correct memory addresses and pins.
In testing, we discovered a problem with our assembler. When assembling a large program
the additional values appended after the instructions appeared in the wrong order.
The issue boiled down to values being added to a symbol table in lexicographical order
as opposed to chronological order. The issue with this was that it lead to integers
being compared using a string comparison method. 

\section{Programming in a group}

\subsection{Communication}

During this project, we used social media to discuss issues, organise meetings,
and announce individual progress with a task. However, most of the time we spent
working, we did so together as a group. This was the best solution as it provided
the opportunity for spontaneous discussion and allowed us to easily track each others
progress. It was also very useful as we could provide a lot of help to each other, and
make sure no-one got left behind. Having begun the project with very little
knowledge of C, we now feel proficient in the language, as not only did we learn
through our own experience in the project, but through the work of our group members
as well by, for example, debugging their code.

One of the issues we had while working in a group is occasionally having trouble
finding a time when all group members were free to meet up, as other commitments
sometimes made this hard. However, we overcame this by agreeing on a regular meeting
time which we could all make, and stuck to it.

\subsection{Delegation of Tasks}

In this project, we decided to tackle one main task per week. We began each task
together as a group to establish common functionality, before then splitting the
work between the four of us based on the four instruction types. We would then regularly
merge our work once a group member had a significant amount of work to submit.
Having initially outlined a series of deadlines for the project, we managed to stick
to them very well, however, having finished the assembler a day later than planned,
with debugging taking longer than expected, and preparation for exams also taking up
some of our individual time, we re-assessed our deadlines and chose to push them back
as we knew that at the pace we were going, we could still complete all the tasks
before the final deadline.

The delegation of tasks was quick and efficient, and when moving on from the emulator
to the assembler, we were able to maintain our "specialist" knowledge of the instruction
types we had previously tackled and apply it to the assembler. We thought it was sensible
to delegate tasks this way as some parts of the project were longer than others, and
thus it would have been unfair to give each member an entire section to complete on
their own. 

\subsection{Next Time...}

Next time when working on a group programming project, we might try to parallelise our
work more so than we did this time. This would reduce the dependence on all team members
being available at the same time, and also possibly speed up the process of the project. Also, as
we may feel a lot more confident in our coding ability, we would require less
support from each other, as we did during this project. On the other hand, we would still maintain regular meetings to ensure the common target is clear to everyone and progression towards the
deadline remains consistent.

We could also make use of tools such as Trello to post key tasks as well as progression
updates when tasks are complete. This is a neat way to communicate advancement to each
other. As well as this, we could utilise the branching system on git more so than we
did this time. This could be useful if we were each working on separate code,
and didn't want our incomplete work in progress to interfere with the work of the other
group members. 

\section{Individual Reflections}

\subsection{Jamshid Barry}
Working with the other members of my group at first seemed a daunting task 
as I had only known one member previously but I wanted to get out of my comfort 
zone and work with people I had never worked with before. This turned out to be 
a great experience as I fitted very well into the group, I made new friendships 
as well as at the same time learnt C and other programming techniques from the 
others. 

Coming into the project I felt that what I could bring to the team would 
be hard work and discipline to sticking to my deadlines and come the end of 
the project I feel like what I contributed to the group is exactly that, 
I did manage to meet my deadlines and if a bug was found within my code 
I quickly worked to remove it and keep to the timings given to us. 

But one weakness I have identified and my group have also identified is that 
when I struggle I tend not to speak up but tackle it on my own or if I don't 
understand something I don't speak up. This is something I can look at myself 
and change for when I work in later group projects and speak up more as an 
active group member. Also at times my code had become very tedious to read, 
littered with comments  code which had been duplicated numerous times. 
I had taken note of that during the checkpoint meeting with Tristan and analyzed 
the WebPA given by my team about me and altered my code and made it alot more 
cleaner and easier to read and follow. One other thing I would also of liked to 
do was the optional part within the emulator for Data Processing instructions.
I feel like my work is incomplete as there is clearly more that can be done to it.
By the presentation I will like to have done functionality mentioned the previously.

\subsection{Chase Hellemans}

As an individual, I am very observant and keen to understand how things work. I feel that
these traits transfered well into this project when it came to debugging my and the other team
members' code - evident from the WebPA feedback. When we started this project, I was worried that my lack of programming
experience in both C and in general would weigh the entire group down. Using this as
pressure on myself, I learnt C as best as I could and am happy on my performance
in both the group work and the Lexis Test as a result.

A weakness of mine is having tunnel vision on a certain approach
when it comes to a problem, which caused delays and possible confusion
for people using my code. Thanks to my team members, there was always someone to give 
advice on another way to solve the issue. Something that I would definitely
improve for my next group project is writing down more ideas and planning ahead for
group meet-ups. Adam very often had a few sheets of paper with possible answers
and I felt that I should be doing something similar as well. 

If I could do this project again, I would take 
longer reading through the spec and familiarising myself with the system before
delving into the programming work as misunderstanding some concepts of the ARM system
made me lose more time overall. I would have also liked to have done the optional shifter
function in Single Data Transfer too, if I had more time. Furthermore, I felt that we 
started programming too early in time with the lectures, as I found myself learning how to 
do a certain approach more efficiently during lectures long after I had attempted it in the 
project. 

Overall, this group project had a very positive outcome to the way I both interact with
a group and the way I program. This was my first experience programming in a group and it turned
out to be a lot more fun and enlightening than I had originally expected. 

\subsection{Adam Wiles}
Throughout the project I have felt comfortable working with all the members of the group. I feel as though I have been able to provide direction to the group and lead without bearing too much control over the group; webPA feedback suggests I have succeeded in this with members being pleased with my "good lead and delegation of work" within the group. As well as taking lead, I have also been able to contribute greatly to the tasks directly, providing ideas and provoking discussion within the group as well as through my individual work away from the group after splitting tasks between members.  

I believe my key strengths lie in taking leadership and getting the most from the group, without creating an overbearing workload, and allowing time for each of us to enjoy time away from work. As a result I think we have made much smoother progress, grown as a group having helped one another and also developed our skills as programmers. I also feel as though my planning skills have proved vital, and developed over the duration of the project; tackling issues such as program architecture, dividing tasks, and setting deadlines which allow quick progression whilst minimising pressure on the group both throughout the project and at the end. At the same time, I have listened to what the group has to say, and try to take points with an open mind as it's easy to develop a 'one-track' mind when working on a solution. Fortunately, I feel as though I have succeeded in this with positive webPA feedback showing the other group members appreciate that I have "listened to what everyone else had to say", ensuring we make the best decisions throughout the project. 

A weakness identified in the webPA is perhaps my desire to reach certain milestones by the end of the day may sometimes lead to fatigue within the group when perhaps a break is more appropriate. Of course, this is a very fine balance between motivation and knowing when to take a break. Having worked on a task for a long period of time, it is sometimes sensible, and more beneficial, to take a break before returning back to the task. 

Overall, the project has been a great success, and I shall take a lot away from it as a learning experience yet what's just as pleasing is the fact that we've been able to have fun as a group over the course of the project. Sticking to the deadlines was definitely intrinsic to our success as a group, and I would definitely create a timeline of short-term targets for any future project. This maintains a focus within the group as well as marking progression, and maintaining a high group morale. In future, I might also set aside time for regular breaks, whether these are used to just relax or for group discussion, it's important not to get bogged down in work, making progress seem slow. However, I will definitely maintain regular meetings, dispelling any issues or concerns quickly and preserving clarity within the group.

\subsection{Oliver Robinson}
This was probably the largest group project I've ever done before, and as such it took
a little while to get used to working in a group on a single program. One of the
largest problems I foresaw was getting all of our methods to work smoothly together,
and trying to understand each other's code so that we could use it effectively in our
own. I thought the best way to do this was discussing clearly about the functions we
made, and what they did. We did just this, however, even so, it was very easy for
someone to change a broken function, affecting the code of all the others. Other than
that (which was pretty unavoidable), working with the rest of the group was very
smooth.  I was lucky enough to have a very relaxed, but at the same time committed and
hard working group. We all stuck well to the agreed timetable, which really helped us
get through the project in a reasonable amount of time. We managed to avoid any major
disagreements over plans for the project, which was one of the major things I hoped to
avoid in a group project such as this. If I were to repeat this project, I would like
to spend a little more time producing an extension. As the rest of the project took us
a while, due to the fact that we were still very much learning the language, we did not
have much of a chance to develop a complex extension, however hopefully with time and
experience, we may be able to fix that next time. 

One thing I found myself doing during this project was, while the rest of the group
was stuck on a particular problem, I would tend to attempt the problem or a different
part of the project alone, then return to the group with a solution or additions to
other parts of the code. An area I think I need to improve on is communication,
especially when I do things like described above, as I sometimes found myself writing
pieces of code which my other team members had already written and got working by the
time I finish. This was brought to light thanks to the webPA comments I received.

\section{Potential Future Extensions}

As we created our extension, we noticed that the technique we used was very systematic
in the way it chose which lights to turn on, and that it would be quite possible to
create a program which could take input from a user which specified the light pattern,
and generate a corresponding assembly source file. We were thinking of using C macro functions to allow us to generate the appropriate
loop sequence, using the given numbers from the user, and place it in the file which
will, in turn, be passed to the assembler, and used to generate the binary instruction
file for the Pi. We are currently not sure as to whether this will be finished in time, 
but we may well complete this after the deadline.
\newline
\newline
\noindent\textbf{EDIT: Recently Completed}\newline
Fortunately, we managed to put together our ideas and complete the extension proposed above just before the deadline. 
We chose to make use of a C macro function to produce each instruction as requested by the user. After accepting
the ARM assembly source file output name as a command line argument we have managed to generate not only the ARM assembly
program, but we then call our assembler to generate a binary file with the default name "lightSequence.img". We first prepare
the file by producing the standard methods which occur before the main loop and having received the input from the user we then 
add the individual methods for turning the corresponding pins on and off. The name of the file is LightSequenceGenerator.c in the
src directory. As well as this, we are now working on getting the optional functions working for the assembler and emulator and aim to 
have these done soon.

\end{document}
