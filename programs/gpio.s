ldr r0,=0x20200004
mov r2,#1
lsl r2,#18
str r2,[r0]

loop:
mov r7,#0xFF0000
await:
sub r7,r7,#1
cmp r7,#0x00
bne await

ldr r0,=0x20200028
mov r2,#1
lsl r2,#16
str r2,[r0]

mov r7,#0xFF0000
wait2:
sub r7,r7,#1
cmp r7,#0x00
bne wait2

ldr r0,=0x2020001c
mov r2,#1
lsl r2,#16
str r2,[r0]
b loop
andeq r0,r0,r0
