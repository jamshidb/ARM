ldr r0,=0x20200004  ;Sets up pin 17 as output
mov r2,#1
lsl r2,#21
str r2,[r0]

ldr r0,=0x20200008 ;sets up pin 27 as output
mov r2,#1
lsl r2,#21
str r2,[r0]

ldr r0,=0x20200000 ;sets up pin 2 as output
mov r2,#1
lsl r2,#6
str r2,[r0]


mov r3,#0
mov r4,#0

switch:

mov r7,#0x1D0000 ;Creates a delay between turning lights off and on
wait1:
sub r7,r7,#1
cmp r7,#0x00
bne wait1

ldr r0,=0x20200028 ;Clears pin 2
mov r2,#1
lsl r2,#2
str r2,[r0]

ldr r0,=0x20200028 ;Clears pin 17
mov r2,#1
lsl r2,#17
str r2,[r0]

ldr r0,=0x20200028 ;Clears pin 27
mov r2,#1
lsl r2,#27
str r2,[r0]

cmp r4,#3
ble count
b pattern

count:

cmp r3,#0
beq light0
cmp r3,#1
beq light1
cmp r3,#2
beq light2
cmp r3,#3
beq light3
cmp r3,#4
beq light4
cmp r3,#5
beq light5
cmp r3,#6
beq light6
cmp r3,#7
beq light7
mov r3,#0

add r4,r4,#1
cmp r4,#3
ble count

mov r3,#8

pattern:
cmp r3,#8
beq light5
cmp r3,#9
beq light2
cmp r3,#10
beq light5
cmp r3,#11
beq light2
cmp r3,#12
beq light1
cmp r3,#13
beq light2
cmp r3,#14
beq light4
cmp r3,#15
beq light0
cmp r3,#16
beq light4
cmp r3,#17
beq light2
cmp r3,#18
beq light1
cmp r3,#19
beq light7
cmp r3,#20
beq light0
cmp r3,#21
beq light7
cmp r3,#22
beq light0
cmp r3,#23
beq light7

mov r3,#0
mov r4,#0
b switch



light0:
ldr r0,=0x20200028 ;Clears pin 2
mov r2,#1
lsl r2,#2
str r2,[r0]

ldr r0,=0x20200028 ;Clears pin 17
mov r2,#1
lsl r2,#17
str r2,[r0]

ldr r0,=0x20200028 ;Clears pin 27
mov r2,#1
lsl r2,#27
str r2,[r0]

add r3,r3,#1
b switch

light1:
ldr r0,=0x2020001c ;Sets pin 17 (1)
mov r2,#1
lsl r2,#17
str r2,[r0]

add r3,r3,#1
b switch

light2:


ldr r0,=0x2020001c ;Sets pin 27 (2)
mov r2,#1
lsl r2,#27
str r2,[r0]

add r3,r3,#1
b switch


light3:

ldr r0,=0x2020001c ;Sets pin 17 (3)
mov r2,#1
lsl r2,#17
str r2,[r0]

ldr r0,=0x2020001c ;Sets pin 27 (2)
mov r2,#1
lsl r2,#27
str r2,[r0]

add r3,r3,#1
b switch

light4:
 
ldr r0,=0x2020001c ;Sets pin 2 (4)
mov r2,#1
lsl r2,#2
str r2,[r0]

add r3,r3,#1
b switch


light5:


ldr r0,=0x2020001c ;Sets pin 17 (5) ;Start of second half
mov r2,#1
lsl r2,#17
str r2,[r0]

ldr r0,=0x2020001c ;Sets pin 2 (4)
mov r2,#1
lsl r2,#2
str r2,[r0]


add r3,r3,#1
b switch


light6:


ldr r0,=0x2020001c ;Sets pin 27 (6)
mov r2,#1
lsl r2,#27
str r2,[r0]

ldr r0,=0x2020001c ;Sets pin 2 (4)
mov r2,#1
lsl r2,#2
str r2,[r0]

add r3,r3,#1
b switch


light7:

ldr r0,=0x2020001c ;Sets pin 17 (7)
mov r2,#1
lsl r2,#17
str r2,[r0]

ldr r0,=0x2020001c ;Sets pin 27 (6)
mov r2,#1
lsl r2,#27
str r2,[r0]

ldr r0,=0x2020001c ;Sets pin 2 (4)
mov r2,#1
lsl r2,#2
str r2,[r0]

add r3,r3,#1
b switch

andeq r0,r0,r0
