#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "symbolTable.h"
#include <ctype.h>
#include "assemble.h"

//Contains mappings for the mnemonics t the int code
struct table *condTable;
struct table *postTable;
int totalBinBytes;
char *binFilePath;

int main(int argc, char **argv) {
    if (argc != 3) {
		perror("Please specify the assembly instructions and binary file respectively");
		exit(EXIT_FAILURE);
		
	}

   	 totalBinBytes = 0;
    
	struct table *symbolTable = tableInit();
	struct table *opcodeTable = tableInit();
	
	char *instrFileName = argv[1];
	char *binFileName = argv[2];

    	binFilePath = binFileName;
	
    	char *firstPassInstructions = firstPass(instrFileName, symbolTable);
    
    	buildPostTable();
	buildCondTable();
	buildOpcodeTable(opcodeTable);
    
	cleanFile(binFileName);

	FILE *binFile = fopen(binFileName, "ab");
    	assemble(instrFileName, symbolTable, binFile, opcodeTable);

	table_destroy(condTable);
	table_destroy(symbolTable);
	table_destroy(opcodeTable);
    	table_destroy(postTable);
	free(firstPassInstructions);

	fclose(binFile);
}


void writeBin(FILE *binFile, int bin){
	//this function writes one 32 bit word to the file at a time
	
	//fwrite swaps endianness, so bin needs to be in big endian here
	int byte1 = (bin>>24) & 255;
	int byte2 = (bin>>8) & (255<<8);
	int byte3 = (bin<<8) & (255<<16);
	int byte4 = (bin<<24) & (255<<24);
	bin = byte1|byte2|byte3|byte4;
	
	int *binPtr = malloc(1*sizeof(int));
	binPtr[0] = bin;
	
	fwrite(binPtr, 4, 1, binFile);
	
	free(binPtr);
}

void cleanFile(char *fileName){
	
	FILE *file =fopen(fileName, "wb");
	fclose(file);
	
}


char *firstPass(char *fileName, struct table *symbolTable) {
    FILE *fp;

    fp = fopen(fileName, "r");
 
    if(fp == NULL){
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    fseek(fp, 0, SEEK_END);
    int fileSize = ftell(fp);
    rewind(fp);

    char *instructions = calloc(sizeof(char), fileSize + 1);
    fread(instructions, sizeof(char), fileSize, fp);
	instructions[fileSize] = '\0';
    char *ch = strtok(instructions, "\n");

    int address = 0;

    while (ch != NULL) {
	if(isEmpty(ch) || ch[0] == ';'){
            		ch = strtok(NULL, "\n");
            } else if (ch[strlen(ch) - 1] == ':') {
                    ch[strlen(ch)-1] = '\0';
                    table_insert_next(symbolTable, ch, address);
            		ch = strtok(NULL, "\n");
            } else {
            	ch =strtok(NULL, "\n");
            	address += 4;
	       }
    }

    totalBinBytes = address;
    
    fclose(fp);

    return instructions;
}

int dataTransfer(char *command, char *argString, int address, struct table *symbolTable, struct table *opcodeTable) {
	char *holdArgs = malloc(sizeof(char)*511);
	if (holdArgs == NULL) {
		perror("Unable to allocate space for holdArgs");
	}
	strcpy(holdArgs, argString);
    int output = 0x00008004; //Needs 01 on bits 26 and 27 and assume up
    if (!strcmp(command, "ldr")) {
            output = output | 1 << 12;
    } else if (strcmp(command, "str")) {
            perror("DataTransfer: Command not recognised!\n");
    }
    int cond = getValue(condTable, "al");
    output = output | cond; //Adds the condition

    int offset = 0;

    char *baseRegr = strtok(argString, ", ");
    strcpy(baseRegr, baseRegr+1); //remove R
    int baseRegNo = atoi(baseRegr);
    baseRegNo <<= 20;
    output = output | baseRegNo;

    char *secondArg = strtok(NULL, ",");
    
    if (secondArg[0] == ' ') {
            //Need to remove space after , if it exists
            strcpy(secondArg, secondArg+1);
    }

    char *thirdArg = NULL;
    char *fourthArg = NULL;
    char *fifthArg = NULL;
    char *thirdArgHold = malloc(511);

    if (secondArg[0] == '=') {

        strcpy(secondArg, secondArg+1);
        int secondArgVal = strtol(secondArg, NULL, 0);
        if (secondArgVal <= 0xff) {
		int mov = dataProcessing("mov", holdArgs, address, symbolTable, opcodeTable);
		free(thirdArg);
		free(holdArgs);
		return mov;
                
        } else {
		int *addressPtr = malloc(sizeof(int));
		*addressPtr = address;
		/*As address is a local variable, it gets freed after this function ends.
		 *This mallocation of a single int is necessary to store it into the postTable
		 *for later use. Not putting it on the heap causes it to be freed and lost,
		 *meaning one cannot use &address when adding it to the table.
		 *This will get freed when the postTable is destroyed.
		 */
                secondArgVal = (((secondArgVal >> 24) & 0xff) | ((secondArgVal << 8) & 0xff0000) 
                                 | ((secondArgVal >> 8) & 0xff00) | ((secondArgVal << 24)& 0xff000000));
                table_insert_back(postTable, addressPtr, secondArgVal);
                offset = totalBinBytes - address - 8;               
                totalBinBytes += 4;
                secondArg = "15";
                output = output | 1;
        }
    } else if (secondArg[0] == '[') {
            thirdArg = strtok(NULL, "]");
            if (secondArg[strlen(secondArg) -1] != ']') {
                    strcpy(secondArg, secondArg+2);
                    //There is no third argument
                    output = output | 1;
                    if (thirdArg == NULL) {
                            offset = 0;
                    } else {
		        strcpy(thirdArgHold, thirdArg);
			fourthArg = strtok(thirdArg, ",");
 			if (!strcmp(thirdArgHold, fourthArg)) {
				  //Not the optional bit
        	                if (thirdArg[0] == ' ') {
                	          //remove whitespace;
	                          strcpy(thirdArg, thirdArg+1);
        	                }
                	        if (thirdArg[0] == 'r') {
                        	  output |= 2;
	                        }
		        	if (thirdArg[1] == '-') {
				  output &= ~(0x00008000);
				  strcpy(thirdArg, thirdArg+1);
				}
                         offset = strtol(thirdArg+1, NULL, 0);
			} else {
			//optional bit
			output |= 2;
			fifthArg = strtok(NULL, " ");
			char *sixthArg = strtok(NULL, " ");
			if (sixthArg[0] == '#') {
				int test = atoi(sixthArg+1);
				offset |= (test << 7);
				printf("Test: %i\n", test);
			} else if (sixthArg[0] == 'r') {
				offset |= 0x10;
				offset |= (atoi(sixthArg+1) << 8);
			} else {
				perror("Optional data transfer function not recognised\n");
			}

			if (!strcmp(fifthArg, "lsl")) {
				//00 is the shift so do nothing
			} else if (!strcmp(fifthArg, "lsr")) {
				//01 is the shift
				offset |= (1 << 5);
			} else if (!strcmp(fifthArg, "asr")) {
				//10 is the shift
				offset |= (2 << 5);
			} else if (!strcmp(fifthArg, "ror")) {
				offset |= (3 << 5);
			} else {
				perror("Rotation not recognised\n");
			}
			int thirdArgTest = atoi(thirdArg+1);
			offset |= thirdArgTest; 
			
                   }
		}
            } else {
                    //removing the brackets and r from base reg
                    strcpy(secondArg, secondArg+2);
                    secondArg[strlen(secondArg)-1] = '\0';
                    if (thirdArg[0] == ' ') {
                            strcpy(thirdArg, thirdArg+1);
                            //remove the space
                    }
                    if (thirdArg[0] == 'r') {
                            output |= 2;
                            //shifted register
                            offset = atoi(thirdArg+1);
                    } else if (thirdArg[0] == '#') {
			offset = atoi(thirdArg+1);
		    } else {
			if (thirdArg[0] == '-') {
			output &= ~(0x00008000);
			strcpy(thirdArg, thirdArg+1);
		    }
                    offset = strtol(thirdArg, NULL, 0);
             }
	}
	} else {
            perror("Command not recognised for dataTransfer!\n");
    }
    if (secondArg[strlen(secondArg)-1] == ']') {
            secondArg[strlen(secondArg)-1] = '\0';
    }
    output = output | (atoi(secondArg) & 15) << 8;

    int off1 = (offset >> 8) & 15;
    int off2 = offset & 255;

    output = output | off2 << 24;
    output = output | off1 << 16;

    free(holdArgs);
    free(thirdArgHold);

    return output;
}

void buildCondTable(void){
    condTable = tableInit();
    table_insert_next(condTable, "eq", 0x0);
    table_insert_next(condTable, "ne", 0x00000010);
    table_insert_next(condTable, "ge", 0x000000A0);
    table_insert_next(condTable, "lt", 0x000000B0);
    table_insert_next(condTable, "gt", 0x000000C0);
    table_insert_next(condTable, "le", 0x000000D0);
    table_insert_next(condTable, "al", 0x000000E0);
}


void buildOpcodeTable(struct table *opcodeTable){
	table_insert_next(opcodeTable, "and", 0);
	table_insert_next(opcodeTable, "eor", 1);
	table_insert_next(opcodeTable, "sub", 2);
	table_insert_next(opcodeTable, "rsb", 3);
	table_insert_next(opcodeTable, "add", 4);
	table_insert_next(opcodeTable, "tst", 8);
	table_insert_next(opcodeTable, "teq", 9);
	table_insert_next(opcodeTable, "cmp", 10);
	table_insert_next(opcodeTable, "orr", 12);
	table_insert_next(opcodeTable, "mov", 13);
}

void buildPostTable(void) {
        postTable = tableInit();
}

void assemble(char *fileName, struct table *symbolTable, FILE *binFile, struct table *opcodeTable){
	FILE *fp = fopen(fileName, "r");

	int lineNo = 0;

	char buf[511];
	char *hold = malloc(sizeof(char)*511);

	while (fgets(buf, sizeof(buf), fp) != NULL){

		strcpy(hold, buf);

		if (buf[0] == ';') {
			continue;
		}
		

		hold = strtok(hold, ";");

		bool isALabel = false;
		//Checks if line is just white space
		if(!isEmpty(buf)){

			char *token = strtok(hold, " ");
			char *args = strtok(NULL, "\0");


			if (args == NULL) {
				isALabel = true;
			}


		if (!isALabel) {

		uint32_t binCode = chooseFunc(token, args, lineNo, symbolTable, opcodeTable);
		writeBin(binFile, binCode);
		lineNo += 4;

		}

		}

	}


	table_iter iterator = table_begin(postTable);
	while (iterator != table_end(postTable)) {
		int offset = table_iter_value(iterator);
		free(table_iter_key(iterator));		
		writeBin(binFile, offset);
		iterator = table_iter_next(iterator);
		}

	free(hold);

	fclose(fp);
}



uint32_t chooseFunc(char *command, char* args, int address, struct table *symbolTable, struct table *opcodeTable){
	uint32_t binCode = 0;
	
	if (!strcmp(command,"mul")) {
		//call multiply
		binCode = multiply(command, args);
	}
	else if (!strcmp(command,"mla")) {
		//call multiply
		binCode = multiply(command, args);
	}
	
	//SingleDataTransfer
	else if (!strcmp(command,"ldr")) {
    	binCode = dataTransfer(command, args, address, symbolTable, opcodeTable);
	} 
	else if (!strcmp(command,"str")) {
		binCode = dataTransfer(command, args, address, symbolTable, opcodeTable);
	}
	
	//Branch
	else if (command[0]=='b'){
		//call Branch
		binCode = branch(command, args, address, symbolTable);

	}
	
	//Special
	else if (!strcmp(command,"lsl")){
		//Left shift
        char *regn = strtok(args, ", ");
        char *amount = strtok(NULL, " ");
        char regNum[] = "   ";
        char amountNum[] = "          ";
        strncpy(regNum, regn + 1, strlen(regn) -1);
        strncpy(amountNum, amount + 1, strlen(amount) - 1);
        binCode = makeLsl(regNum, amountNum);
	}
	else if (!strcmp(command,"andeq")){
		binCode = 0x00000000;
	}
	
	//Data processing
	else {
		//Call data processing
		binCode = dataProcessing(command, args, address, symbolTable, opcodeTable);
	}	
	return binCode;
}

int dataProcessing(char *command, char *args, int address, struct table *symbolTable, struct table *opcodeTable){
	uint32_t instr = 0;
	int cond = 14;
	int opCode = getValue(opcodeTable, command);
	int i = 0;
	int s = 0;
	int rd = 0;
	int rn = 0;
	char *operand2 = NULL;
	uint32_t operand2Val = 0;


	//Single operand assignment - mov Rd, <Operand2>
	if(!strcmp(command, "mov")){
		char *dest;
		dest = strtok(args, ", ");
		char destTemp[] = "          ";
		strncpy(destTemp, dest + 1, strlen(dest) - 1);
		rd = atoi(destTemp);
		operand2 = strtok(NULL, " ");
	

	//Instructions that do not compute results - <opcode> Rn, <Operand2>
	}else if(!strcmp(command, "tst") || !strcmp(command, "teq") || 
		!strcmp(command, "cmp")){
		char *regN;
		regN = strtok(args, ", ");
		char regNTemp[] = "          ";
		strncpy(regNTemp, regN + 1, strlen(regN) - 1);		
		rn = atoi(regNTemp);

		operand2 = strtok(NULL, " ");
		s = 1;


	//Instructions that do compute results - <opcode> Rd, Rn, <Operand2>
	}else{
		char *dest;
		dest = strtok(args, ", ");
		char destTemp[] = "   ";
		strncpy(destTemp, dest + 1, strlen(dest) - 1);
		rd = atoi(destTemp);

		
		char *regN;
		regN = strtok(NULL, ", ");
		char regNTemp[] = "   ";
		strncpy(regNTemp, regN + 1, strlen(regN) - 1);
		rn = atoi(regNTemp);

		operand2 = strtok(NULL, " ");
	}


	//Calculating int value of Operand2, getting first char of Operand2
	char fstCharOp[] = " ";
	strncpy(fstCharOp, operand2, 1);
	//Immediate Value:
	if(!strcmp(fstCharOp, "#") || !strcmp(fstCharOp, "=")){
		if (operand2[2] == 'x') {
			operand2Val = (int) strtol(operand2+1, NULL, 0);
			operand2Val = findImmediateValue(operand2Val);
		} else {
			operand2Val = findImmediateValue(atoi(operand2+1));
		}
		i = 1;

	//Operand2 is a register
	}else{
		char reg[] = "   ";
		strncpy(reg, operand2 + 1, 1);
		int32_t rm = atoi(reg);
		operand2Val = rm;
	}
	printf("Operand2: %s, Val: %04x\n", operand2, operand2Val);

	if (operand2[strlen(operand2) - 1] == ',') {
		printf("Entering optional bit\n");
		char *thirdArg = strtok(NULL, " ");
		char *fourthArg = strtok(NULL, "\n");
		printf("%s, %s\n", thirdArg, fourthArg);

		int rotateInt = 0;

		if (!strcmp(thirdArg, "lsl")) {
			//keep as 0
		} else if (!strcmp(thirdArg, "lsr")) {
			rotateInt = 1;
		} else if (!strcmp(thirdArg, "asr")) {
			rotateInt = 2;
		} else if (!strcmp(thirdArg, "ror")) {
			rotateInt = 3;
		} else {
			perror("Rotate command not recognised for dataProcessing!\n");
		}

		operand2Val |= (rotateInt) << 5;

		if (fourthArg[0] == 'r') {
			printf("Shifted register\n"); 
			operand2Val |= (atoi(fourthArg+1) << 8);
			operand2Val |= (1 << 4);
		} else if ((fourthArg[0] == '#') && (fourthArg[2] == 'x')) {
			printf("Shifted by constant\n");
			operand2Val |= (strtol(fourthArg+1, NULL, 0) << 8);
		} else if (fourthArg[0] == '#') {
			operand2Val |= (atoi(fourthArg+1) << 8);
		}
	}


	cond <<= 28;
	i <<= 25;
	opCode <<= 21;
	s <<= 20;
	rn <<= 16;
	rd <<= 12;
	instr = (cond) | (i) | (opCode) | (s) | (rn) | (rd) | (operand2Val);


	uint32_t b1 = (0x000000ff & instr) << 24;
	uint32_t b2 = (0x0000ff00 & instr) << 8;
	uint32_t b3 = (0x00ff0000 & instr) >> 8;
	uint32_t b4 = (0xff000000 & instr) >> 24;
	instr = b1 | b2 | b3 | b4;

	return instr;
}

uint32_t findImmediateValue(uint32_t op2){
	uint32_t rotate = 0;
	uint32_t immVal = op2;
	while((immVal > 255 && rotate < 32) || (rotate % 2 != 0)){
		immVal = (immVal << 1) | (immVal >> 31);
		rotate += 1;
	}

	if(rotate > 30){
		perror("The numeric constant cannot be represented");
		exit(EXIT_FAILURE);
	}
	immVal = immVal | ((rotate/2) << 8);	
	return immVal;
}

uint32_t branch(char *command, char *args, int address, struct table *symbolTable){
    char *cond = calloc(sizeof(char), 2);
    if(cond == NULL){
        perror("Unable to allocate memory in branch");
        exit(EXIT_FAILURE);
    }
    
    if(strlen(command) > 1){
        strncpy(cond, command + 1, 2);
    } else {
        strncpy(cond, "al", 2);
    }
    uint32_t condCode = getValue(condTable, cond);
    
    //Gets label and places a '\0' at the end of the string
    char *label = strtok(args, " ");
    label[strlen(label) - 1] = '\0';
    int labelAddress = getValue(symbolTable, label);
    
    int32_t offset = labelAddress - address;
   
	offset -= 8;
    offset >>= 2;
    
    offset &= 0x00ffffff;
    
	//Re-arrange bytes into little endian
    uint32_t lB = (offset & 0x000000ff) << 24;
    uint32_t mB = (offset & 0x0000ff00) << 8;
    uint32_t hB = (offset & 0x00ff0000) >> 8;
    uint32_t instruction = lB | mB | hB| condCode | 0x0000000A;

    free(cond);
    
    return instruction;

}


int multiply(char *command, char* args){
	int result = 0x900000E0; //Sets cond = 1110, and other predetermined bits 	
	
	if (strcmp(command, "mul") == 0) {
		char *token = strtok(args, ",");
		int destReg = (token[1]-'0');
		token = strtok(NULL, ",");
		int firstArg = (token[1]-'0');
		token = strtok(NULL, ",");
		int secArg = (token[1]-'0');
		
		destReg  <<= 8;
		firstArg <<=24;
		secArg <<= 16;
		
		result |= destReg|firstArg|secArg;
		
	}else {
		char *token = strtok(args, ",");
		int destReg = (token[1]-'0');
		token = strtok(NULL, ",");
		int firstArg = (token[1]-'0');
		token = strtok(NULL, ",");
		int secArg = (token[1]-'0');
		token = strtok(NULL, ",");
		int thirdArg = (token[1]-'0');
		
		destReg  <<= 8;
		firstArg <<= 24;
		secArg <<= 16;
		thirdArg <<= 20;
		int acc = 1<<13;
		
		result |= destReg|firstArg|secArg|thirdArg|acc;
	}

	return result;
}

int makeLsl(char *regNo, char *shiftAmount){
    //regNo should be 0 - 15 (4 bits)
    uint32_t reg = atoi(regNo);
    uint32_t shift;
    if(strlen(shiftAmount) > 2){
	if(shiftAmount[1] == 'x'){
		shift = strtol(shiftAmount, NULL, 16);
	}else{
		shift = strtol(shiftAmount, NULL, 10);
	}
    }else{
	shift = strtol(shiftAmount, NULL, 10);
    }
    uint32_t binCode;
    //Fills the bottom 11 bits with the shift amount followed by shift type code
    //and register to shift - Currently in big endian.
    //Also ensures operand2 only takes up the bottom 11 bits
    uint32_t operand2 = ((shift << 7) | reg) & ((1 << 12) - 1);
    //printf("Operand2 : %i", operand2);
    //Manually asembling the binary instruction - turned into a move function
    //cond = always, I = 0, S = 1, etc.
    //Formed in big endian
    //binCode = 0xe1a00000 | (reg << 16) | (reg << 12) | operand2;
    //Below produces test case output but above is actually correct
    binCode = 0xe1a00000 | (reg << 12) | operand2;
    
    //Re-arranging into little-endian
    uint32_t b1 = (0x000000ff & binCode) << 24;
    uint32_t b2 = (0x0000ff00 & binCode) << 8;
    uint32_t b3 = (0x00ff0000 & binCode) >> 8;
    uint32_t b4 = (0xff000000 & binCode) >> 24;
    binCode = b1 | b2 | b3 | b4;
    return binCode;
}

//Checks if a line is just whiteSpace
int isEmpty(char *string) {

	if (string == NULL) {
		return 1;
	}
	int i = 0;
	while(string[i] != '\0'){
		if(!isspace(string[i])){
			return 0;
		}
		i++;
	}
	return 1;
}

