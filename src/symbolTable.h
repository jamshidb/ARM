#ifndef _symbolTable_h
#define _symbolTable_h

//Alias for iterator over table
typedef struct table_elem* table_iter;

struct table_elem* newTableElem(void);
void freeTableElem(struct table_elem *elem);
struct table *tableInit(void);

//Both of these return an iterator pointing to the start or end of the
//table respectively
table_iter table_begin(struct table *t);
table_iter table_end(struct table *t);

void table_insert(struct table *t, table_iter iter, void *key, int value);
table_iter table_iter_next(table_iter iter);
int table_iter_value(table_iter iter);
char *table_iter_key(table_iter iter);
int elem_is_internal(table_iter iter);
void table_insert_front(struct table *t, void *key, int value);
void table_insert_back(struct table *t, void *key, int value);

//Free's memory allocated to the table
void table_destroy(struct table *t);

//Inserts the element in lexicographical order into the table
//NOTE: If key already exists nothing is inserted
void table_insert_next(struct table *t, void *key, int value);

void testOrderedTable(void);

//Returns 1 iff the key already exists, else returns 0;
int keyExists(struct table *t, char *key);

//Returns the value associated with key.
//Pre: Key already exists in the table
int getValue(struct table *t, char *key);


#endif
