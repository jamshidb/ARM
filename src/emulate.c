#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "emulate.h"

enum funcType {
	DATA_PROCESSING, MULTIPLY, DATA_TRANSFER, BRANCH
};

struct chipState {
	//ARM has 17 registers - But can ignore 2 - registers[17]
	//Registers 0 - 12 are general purpose
	int32 *registers;
	uint8_t *mainMemory;
	int32 *pc; //= registers + 15;
	int32 *cpsr; //= registers + 16;
};

struct chipState armSystem;
bool hasStopped;

int main(int argc, char **argv) {
	initialise();
	int32 *info = malloc(sizeof(int32) * 10);
	hasStopped = false;
	bool firstTime = true;

	char *fileName = argv[1];
	read(fileName);

	//debug();

	int32 nxtInstr = fetch();

	while (!hasStopped) {
		if (!firstTime) {
			int res = execute(info);
			//res == 1 iff instruction executed was a branch statement
			//so fetch again before next decode
			if (res == 1) {
				nxtInstr = fetch();
			}
		}

		if (firstTime)
			firstTime = false;

		decode(nxtInstr, info);
		nxtInstr = fetch();
	}

	printState();

	free(info);
	free(armSystem.registers);
	free(armSystem.mainMemory);

	return EXIT_SUCCESS;
}

//Reads in data from binary file and places it in memory
void read(char *fileName) {
	FILE *pFile;

	pFile = fopen(fileName, "rb");

	if (pFile == NULL) {
		perror("Error opening file\n");
		return;
	}

	fseek(pFile, 0, SEEK_END);
	int fileSize = ftell(pFile);
	rewind(pFile);

	fread(armSystem.mainMemory, sizeof(int8_t), fileSize, pFile);

	fclose(pFile);
}

//Allocating and intialising memory space to 0
void initialise(void) {
	armSystem.mainMemory = calloc(65536, sizeof(int8_t));

	if (armSystem.mainMemory == NULL) {
		perror("Memory allocation failed");
		return;
	}

	armSystem.registers = calloc(17, sizeof(int32));
	armSystem.pc = armSystem.registers + 15;
	armSystem.cpsr = armSystem.registers + 16;

	if (armSystem.registers == NULL) {
		perror("Register initialisation failed");
		exit(EXIT_FAILURE);
	}
}

int32 fetch(void) {
	uint32_t b0 = armSystem.mainMemory[*(armSystem.pc)];
	uint32_t b1 = armSystem.mainMemory[*(armSystem.pc) + 1];
	uint32_t b2 = armSystem.mainMemory[*(armSystem.pc) + 2];
	uint32_t b3 = armSystem.mainMemory[*(armSystem.pc) + 3];
	b0 <<= 24;
	b1 <<= 16;
	b2 <<= 8;
	int32 instruction = b0 | b1 | b2 | b3;
	*(armSystem.pc) += 4;
	return instruction;
}

int32 getInstructionAtLocation(int location) {
	uint32_t b0 = armSystem.mainMemory[location];
	uint32_t b1 = armSystem.mainMemory[location + 1];
	uint32_t b2 = armSystem.mainMemory[location + 2];
	uint32_t b3 = armSystem.mainMemory[location + 3];
	b0 <<= 24;
	b1 <<= 16;
	b2 <<= 8;
	int32 instruction = b0 | b1 | b2 | b3;
	return instruction;
}

void decode(int32 instr, int *info) {
	//Check if its a stop instruction i.e. all 0's instruction
	if (instr == 0) {
		hasStopped = true;
		return;
		//exit(EXIT_SUCCESS);
	}

	int32 type = (instr >> 2);
	int mask = 3;
	int bit25;
	int mask25;

	switch (type & mask) {

	case 0: //horrible case
		bit25 = instr >> 1;
		mask25 = 1;

		if ((bit25 & mask25) == 1) {
			//Call Data processing
			dataProcessingInfo(instr, info);
			break;
		} else {
			int bit4 = instr >> 28;
			int mask4 = 1;
			if ((bit4 & mask4) == 0) {
				//call Data processing
				dataProcessingInfo(instr, info);
				break;
			} else {
				int bit7 = instr >> 31;
				int mask7 = 1;
				if ((bit7 & mask7) == 1) {
					//call Multiply
					info[0] = MULTIPLY;
					int cond = ((instr >> 4) & 15);
					int8_t acc = ((instr >> 13) & 1);
					int8_t setCond = ((instr >> 12) & 1);
					int8_t destReg = ((instr >> 8) & 15);
					int8_t opRegN = armSystem.registers[((instr >> 20) & 15)];
					int8_t opRegS = armSystem.registers[((instr >> 16) & 15)];
					int8_t opRegM = armSystem.registers[((instr >> 24) & 15)];

					info[1] = cond;
					info[2] = acc;
					info[3] = setCond;
					info[4] = destReg;
					info[5] = opRegN;
					info[6] = opRegS;
					info[7] = opRegM;

					break;

				} else {
					//call data processing
					dataProcessingInfo(instr, info);
					break;
				}
			}
		}
		break;

	case 1: { //call single data transfer
		uint8_t cond = ((instr >> 4) & 15);
		uint8_t offsetBit = (instr >> 1) & 1;
		uint8_t indexingBit = instr & 1;
		uint8_t upBit = (instr >> 15) & 1;
		uint8_t loadBit = (instr >> 12) & 1;
		uint8_t baseReg = (instr >> 8) & 15;
		uint8_t destReg = (instr >> 20) & 15;
		uint8_t off1 = (instr >> 16) & 15;
		uint8_t off2 = (instr >> 24) & 255;
		uint16_t offset = (off1 << 8) | off2;
		info[0] = DATA_TRANSFER;
		info[1] = cond;
		info[2] = offsetBit;
		info[3] = indexingBit;
		info[4] = upBit;
		info[5] = loadBit;
		info[6] = baseReg;
		info[7] = destReg;
		info[8] = offset;
	}
		break;

	case 2: //Call branch
	{
		int32 cond = (instr >> 4) & 15;
		uint32_t bLow = (instr >> 24) & 255;
		uint32_t bMid = ((instr >> 16) & 255) << 8;
		uint32_t bHigh = ((instr >> 8) & 255) << 16;
		uint32_t extendedSign = 0xff000000;
		if ((bHigh >> 7) == 0) {
			extendedSign = 0x00;
		}
		int32 offset = extendedSign | bLow | bMid | bHigh;

		info[0] = BRANCH;
		info[1] = cond;
		info[2] = offset;
	}
		break;

	default:
		perror("Invalid instruction");
	}
}

//Filling the info array with required information for execution
void dataProcessingInfo(uint32_t instr, int *info) {

	int cond = (instr >> 4) & 15;
	int i = ((instr >> 1) & 1);
	int opCode = ((instr & 1) << 3) | ((instr >> 13) & 7);
	int s = ((instr >> 12) & 1);
	//Source register
	int Rn = ((instr >> 8) & 15);
	//Destination register
	int Rd = ((instr >> 20) & 15);
	int operand2 = ((instr >> 8) & 3840) | ((instr >> 24) & 255);

	info[0] = DATA_PROCESSING;
	info[1] = cond;
	info[2] = i;
	info[3] = opCode;
	info[4] = s;
	info[5] = Rn;
	info[6] = Rd;
	info[7] = operand2;

}

void multiply(int cond, int8_t acc, int8_t setCond, int8_t destReg,
		int8_t opRegN, int8_t opRegS, int8_t opRegM) {


	if (checkCond(cond)) {

		int32 res = 0;

		//Checks if instruction is a multiply-accumulate instruction or not
		if (acc == 1) {
			res = (opRegM * opRegS) + opRegN;
			armSystem.registers[destReg] = res;
		} else {
			res = opRegM * opRegS;
			armSystem.registers[destReg] = res;
		}

		if (setCond == 1) {
			//Setting flag N equal to 31st bit of result (i.e. if res was 
			// negative) and Z to one if result was zero
			*armSystem.cpsr |= (res >> 31) << 7;

			if (res == 0) {
				*armSystem.cpsr |= 1 << 6;
			}
		}
	}
}


void dataProcessing(int cond, int i, int opCode, int s, int Rn, int Rd,
		int operand2) {
	int calcOperand2 = 0;
	int cFlag = 0;

	//I flag = 1 -> Operand2 is an immediate value
	if (i) {
		int rotate = (operand2 >> 8) * 2;
		uint32_t Imm = operand2 & 255;
		calcOperand2 = rotateRight(Imm, rotate);

	} else {
		//I flag = 0 -> Operand2 is a register
		//Rm could be a referring to PC - Spec says we can assume it won't
		int Rm = operand2 & 15;
		//Gets bit 4 of operand
		//int bit4 = operand2 & 16; 
		int bit4 = (operand2 >> 4) & 1;
		int shift = operand2 >> 5;
		uint8_t integerShift = 0;

		if (bit4 == 0) {
			integerShift = (operand2 >> 7) & (31); //2^5 - 1
		} else {
			int shifterRegister = operand2 >> 8;
			integerShift = armSystem.registers[shifterRegister];
		} 

			switch (shift & 3) {
			//Logical shift left
			case 0:
				cFlag = (armSystem.registers[Rm] >> (31 - integerShift)) & 1;
				calcOperand2 = logicalShiftLeft(armSystem.registers[Rm], integerShift);
				break;

				//Logical shift right
			case 1:
				cFlag = (armSystem.registers[Rm] >> (integerShift - 1)) & 1;
				calcOperand2 = logicalShiftRight(armSystem.registers[Rm], integerShift);
				break;

				//Arithmetic shift right
			case 2:
				cFlag = (armSystem.registers[Rm] >> (integerShift - 1)) & 1;
				calcOperand2 = arithmaticShiftRight(armSystem.registers[Rm], integerShift);
				break;

				//Rotate right
			case 3:
				calcOperand2 = rotateRight(armSystem.registers[Rm],
						integerShift);
				cFlag = calcOperand2 & 0x80000000;
				break;

			default:
				perror("Invalid shift");
			}
	}

	if (checkCond(cond)) {
		int res = 0;

		switch (opCode) {
		//Rn AND operand2  - and
		case 0:
			armSystem.registers[Rd] = armSystem.registers[Rn] & calcOperand2;

			if(s){
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;
			//Rn EOR operand2  - eor
		case 1:
			armSystem.registers[Rd] = armSystem.registers[Rn] ^ calcOperand2;

			if (s) {
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;

			//Rn - operand2  - sub
		case 2:
			armSystem.registers[Rd] = armSystem.registers[Rn] - calcOperand2;

			if(s){
				setFlag(CFLAG, !(armSystem.registers[Rn] < calcOperand2));
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;

			//operand2 - Rn  - rsb
		case 3:
			armSystem.registers[Rd] = calcOperand2 - armSystem.registers[Rn];

			if(s){
				setFlag(CFLAG, !(calcOperand2 < armSystem.registers[Rn]));
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;

			//Rn + operand2  - add
		case 4:
			armSystem.registers[Rd] = armSystem.registers[Rn] + calcOperand2;

			res = armSystem.registers[Rd];

			if(s){
				setFlag(CFLAG, (res > 0xFFFFFFFF));
				setFlag(ZFLAG, armSystem.registers[Rd] == 0);
				setFlag(NFLAG, ((armSystem.registers[Rd] >> 31) == 1));
			}
			break;
			//as case 0, but result not written  - tst
		case 8: {
			res = armSystem.registers[Rn] & calcOperand2;
			if(s){
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !res);
				setFlag(NFLAG, (res < 0));
			}
		}
			break;

			//as case 1, but result not written  - teq
		case 9: {
			res = armSystem.registers[Rn] ^ calcOperand2;

			if(s){
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !res);
				setFlag(NFLAG, (res < 0));
			}
		}
			break;

			//as case 2, but result not written  - cmp
		case 10:
			res = armSystem.registers[Rn] - calcOperand2;

			if(s){
				setFlag(CFLAG, !(armSystem.registers[Rn] < calcOperand2));
				setFlag(ZFLAG, !res);
				setFlag(NFLAG, (res < 0));
			}
			break;
			//Rn OR operand2  - orr
		case 12:
			armSystem.registers[Rd] = armSystem.registers[Rn] | calcOperand2;

			if(s){
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;

			//operand2 (Rn is ignored)  - mov
		case 13:

			armSystem.registers[Rd] = calcOperand2;

			if(s){
				setFlag(CFLAG, cFlag);
				setFlag(ZFLAG, !armSystem.registers[Rd]);
				setFlag(NFLAG, (armSystem.registers[Rd] < 0));
			}
			break;
		default:
			perror("Invalid instruction");

		}
	}
}

void setFlag(set_flag whichFlag, bool flag){
	switch(whichFlag){

		case CFLAG: if(flag){*armSystem.cpsr = *armSystem.cpsr | (1 << 29);}
			        else{*armSystem.cpsr = *armSystem.cpsr & ~(1 << 29);}
			        break;
  
		case ZFLAG: if(flag){*armSystem.cpsr = *armSystem.cpsr | (1 << 30);}
			        else{*armSystem.cpsr = *armSystem.cpsr & ~(1 << 30);}
			        break;

		case NFLAG: if(flag){*armSystem.cpsr = *armSystem.cpsr | (1 << 31);}
			        else{*armSystem.cpsr = *armSystem.cpsr & ~(1 << 31);}
			        break;

		default:        break;
	}
}

int rotateRight(int num, int bits) {
	return ((num >> bits) | (num << (32 - bits)));
}

int branch(uint32_t cond, int32 offset) {
	if (!checkCond(cond)) {
		return 0;
	}
	offset <<= 2;
	*(armSystem.pc) += offset;
	return 1;
}

void printState(void) {
	printf("Registers:\n");
	for (int i = 0; i < 13; i++) {
		printf("$%-2i : %10d (0x%08x)\n", i, armSystem.registers[i],
				armSystem.registers[i]);
	}

	printf("PC  : %10d (0x%08x)\n", *(armSystem.pc), *(armSystem.pc));
	printf("CPSR: %10d (0x%08x)\n", *(armSystem.cpsr), *(armSystem.cpsr));

	printf("Non-zero memory:\n");
	for (int i = 0; i < 65536; i += 4) {
		if (getInstructionAtLocation(i) != 0) {
			printf("0x%08x: 0x%08x\n", i, getInstructionAtLocation(i));
		}
	}
}

void dataTransfer(int *info) {
	if (checkCond(info[1])) {

		int offsetBit = info[2];
		int indexingBit = info[3];
		int upBit = info[4];
		int storeBit = info[5];
		int baseReg = info[6];
		int destReg = info[7];
		int offset = info[8];
		int upMult; //Multiplier to set the offset up or down

		if (upBit == 1) {
			upMult = 1;
		} else {
			upMult = -1;
		}

		bool isPre = indexingBit == 1;
		bool isLoad = storeBit == 1;
		bool isShifted = offsetBit == 1;

		if (isShifted) {
			uint32_t shiftReg = offset & 15;
			uint32_t shiftType = (offset >> 5) & 3;
			uint32_t shiftInt = (offset >> 7);
			int registerValue = armSystem.registers[shiftReg];

			if (((offset >> 4) & 1) == 1) {
				int shifterReg = offset >> 8;
				shiftInt = armSystem.registers[shifterReg];
			}
			
			switch (shiftType) {

			case 0:
				offset = logicalShiftLeft(registerValue, shiftInt);
				break;
			case 1:
				offset = logicalShiftRight(registerValue, shiftInt);
				break;
			case 2:
				offset = arithmaticShiftRight(registerValue, shiftInt);
				break;
			case 3:
				offset = rotateRight(registerValue, shiftInt);
				break;
			default:
				perror("Shift type not compatible");
			}

		}

		int displayAddress = armSystem.registers[baseReg];

		if (isPre) {
			displayAddress += offset * upMult;
		}

		if (displayAddress > 65538) {
			switch (displayAddress) {
			case 0x20200000:
			case 0x20200004:
			case 0x20200008:
				enablePin(displayAddress);
				break;
			case 0x2020001c:
			case 0x20200028:
				togglePin(displayAddress);
				return;
			default:
				printf("Error: Out of bounds memory access at address 0x%08x\n",
						displayAddress);
				return;
			}
		}

		if (isLoad) {
			switch (displayAddress) {
			case 0x20200000:
			case 0x20200004:
			case 0x20200008:
				armSystem.registers[destReg] = displayAddress;
				return;
			case 0x20200028:
			case 0x2020001c:
				armSystem.registers[destReg] = displayAddress;
				return;
			default:
				break;
			}

			if (isPre) {
				uint32_t contentAddress = armSystem.registers[baseReg];
				armSystem.registers[destReg] = readContent(
						contentAddress + (upMult * offset));
			} else {
				uint32_t contentAddress = armSystem.registers[baseReg];
				armSystem.registers[destReg] = readContent(contentAddress);
				armSystem.registers[baseReg] = contentAddress
						+ (upMult * offset);
			}
		} else {
			if (isPre) {
				uint32_t memoryAddress = armSystem.registers[baseReg];
				switch (memoryAddress + (upMult * offset)) {
				case 0x20200000:
				case 0x20200004:
				case 0x20200008:
					armSystem.registers[destReg] = memoryAddress
							+ (upMult * offset);
					break;
				default:
					writeContent(armSystem.registers[destReg],
							memoryAddress + (upMult * offset));
					break;
				}
			} else {
				uint32_t memoryAddress = armSystem.registers[baseReg];
				switch (memoryAddress) {
				case 0x20200000:
				case 0x20200004:
				case 0x20200008:
					armSystem.registers[destReg] = memoryAddress;
					break;
				default:
					writeContent(armSystem.registers[destReg], memoryAddress);
					break;
				}
				armSystem.registers[baseReg] = memoryAddress
						+ (upMult * offset);
			}
		}

	}

}
uint32_t readContent(uint32_t leadingAddress) {
	uint32_t byte0, byte1, byte2, byte3;
	byte0 = armSystem.mainMemory[leadingAddress];
	byte1 = armSystem.mainMemory[leadingAddress + 1];
	byte2 = armSystem.mainMemory[leadingAddress + 2];
	byte3 = armSystem.mainMemory[leadingAddress + 3];
	byte3 <<= 24;
	byte2 <<= 16;
	byte1 <<= 8;
	uint32_t output = byte0 | byte1 | byte2 | byte3;
	return output;
}

void writeContent(uint32_t content, uint32_t leadingAddress) {
	uint8_t byte0, byte1, byte2, byte3;
	byte0 = content & 255;
	byte1 = (content >> 8) & 255;
	byte2 = (content >> 16) & 255;
	byte3 = (content >> 24) & 255;
	armSystem.mainMemory[leadingAddress] = byte0;
	armSystem.mainMemory[leadingAddress + 1] = byte1;
	armSystem.mainMemory[leadingAddress + 2] = byte2;
	armSystem.mainMemory[leadingAddress + 3] = byte3;
}

bool checkCond(uint32_t cond) {
	int flagN = (*armSystem.cpsr >> 31) & 1;
	int flagZ = (*armSystem.cpsr >> 30) & 1;
	//int flagC = (armSystem.cpsr >> 5) & 1;
	int flagV = (*armSystem.cpsr >> 29) & 1;
	switch (cond) {
	case 0:
		return (flagZ == 1);
	case 1:
		return (flagZ == 0);
	case 10:
		return (flagN == flagV);
	case 11:
		return (flagN != flagV);
	case 12:
		return (flagZ == 0 && (flagN == flagV));
	case 13:
		return (flagZ == 1 || (flagN != flagV));
	case 14:
		return true;
	default:
		perror("Condition not valid in instruction");
		return false;
	}
}

uint32_t logicalShiftLeft(uint32_t registerValue, int shiftMag) {
	return registerValue << shiftMag;
}

uint32_t logicalShiftRight(uint32_t registerValue, int shiftMag) {
	return registerValue >> shiftMag;
}

uint32_t arithmaticShiftRight(uint32_t registerValue, int shiftMag) {
	int castedValue = registerValue;
	return castedValue >> shiftMag;
}

//Execute returns 1 if the statement was a branching statement
//otherwise returns 0
//info[0] corresponds to function type 0 - 3, corresponding to data processing,
//Multiply, Data transfer, branching
int execute(int *info) {
	//int32 funcT = info[0];
	switch (info[0]) {
	case DATA_PROCESSING:
		//Data processing
		dataProcessing(info[1], info[2], info[3], info[4], info[5], info[6],
				info[7]);
		break;

	case MULTIPLY:
		multiply(info[1], info[2], info[3], info[4], info[5], info[6], info[7]);
		return 0;
		break;

	case DATA_TRANSFER:
		dataTransfer(info);
		break;

	case BRANCH:
		return branch(info[1], info[2]);
	default:
		perror("No such instruction to execute");
	}
	return 0;
}

static void enablePin(uint32_t gpioLocation) {
	switch (gpioLocation) {
	case 0x20200000:
		printf("One GPIO pin from 0 to 9 has been accessed\n");
	/*	int Pin0 = ;
		int Pin1 = ;
		int Pin2 = ;
		int pin3 = ;
		int pin4 = ;
		int pin5 = ;
		int pin6 = ;
		int pin7 = ;
		int pin8 = ;
		int pin9 = ;*/
		break;
	case 0x20200004:
		printf("One GPIO pin from 10 to 19 has been accessed\n");
/*		int Pin10 = ;
		int Pin11 = ;
		int Pin12 = ;
		int pin13 = ;
		int pin14 = ;
		int pin15 = ;
		int pin16 = ;
		int pin17 = ;
		int pin18 = ;
		int pin19 = ;*/
		break;
	case 0x20200008:
		printf("One GPIO pin from 20 to 29 has been accessed\n");
/*		int Pin20 = ;
		int Pin21 = ;
		int Pin22 = ;
		int pin23 = ;
		int pin24 = ;
		int pin25 = ;
		int pin26 = ;
		int pin27 = ;
		int pin28 = ;
		int pin29 = ;*/
		break;
	}
}

static void togglePin(int gpioPinSet) {
	switch (gpioPinSet) {
	case 0x20200028:
		printf("PIN OFF\n");
		break;
	case 0x2020001c:
		printf("PIN ON\n");
		break;
	default:
		break;
	}
}

