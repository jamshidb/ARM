#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>
#include "symbolTable.h"

struct table_elem;


struct table{
	struct table_elem *header;
	struct table_elem *footer;
};

struct table_elem{
	void *key;
	int value;
	struct table_elem* prev;
	struct table_elem* next;
};

///////////////////////////////////////////////////////////////
/*int main(void){
	struct table *myTable = tableInit();
	myTable -> header -> key = "Hi";
	printf("Key: %s\n", myTable -> header -> key);
	add("Hello", 2, myTable);
	printf("Value added: %s\n", myTable -> header -> next -> key);
    table_destroy(myTable);
    //printf("%s\n",myTable -> header -> key);
	return 0;
}*/

/*
int main(void){
    //testOrderedTable();
    struct table *t = tableInit();
    table_insert_front(t, "Adam", 1);
    table_insert_front(t, "Bob", 2);
    table_insert_back(t, "Cat", 1);
    table_insert_back(t, "Aj", 2);
    for(table_iter iter = table_begin(t);
        iter != table_end(t);
        iter = table_iter_next(iter)){
        printf("%i\n", table_iter_value(iter));
    }
    table_destroy(t);*/

void testOrderedTable(void){
    struct table *myTable = tableInit();
    table_insert_next(myTable, "Adam", 1);
    table_insert_next(myTable, "Will", 2);
    table_insert_next(myTable, "Bob", 3);
    
    table_iter iter = table_begin(myTable);
    while(elem_is_internal(iter)){
        printf("Key: %s with value %i\n",
               table_iter_key(iter), table_iter_value(iter));
        iter = table_iter_next(iter);
    }
    
    printf("Does Adam exist? %d\n", keyExists(myTable, "Adam") == 1);
    printf("Does Bob exist? %d\n", keyExists(myTable, "Bob"));
    if(keyExists(myTable, "Will")){
        printf("Value of Will is: %i\n", getValue(myTable, "Will"));
    }
    
    table_destroy(myTable);
}

struct table_elem* newTableElem(void){
	struct table_elem *elem = malloc(sizeof(struct table_elem));
	if(elem == NULL){	
		perror("New table element couldn't be created");
		exit(EXIT_FAILURE);
	}
	return elem;
}

void freeTableElem(struct table_elem *elem){
	free(elem);
}

struct table *tableInit(void){
	struct table *t = malloc(sizeof(struct table));
	t -> header = newTableElem();
	t -> footer = newTableElem();
    t -> header -> next = t -> footer;
    t -> header -> prev = NULL;
    t -> footer -> next = NULL;
	t -> footer -> prev = t -> header;
	return t;
}

/*void add(char *str, int val, struct table *t){
	struct table_elem *new = newTableElem();
	//new -> key = malloc(strlen(str) * sizeof(char));
	new -> key = str;
	new -> value = val;

	new -> next = t -> footer;
	new -> prev = t -> footer -> prev;

	t -> footer -> prev -> next = new;
	t -> footer -> prev = new;
}*/

//Returns a pointer to the first element of the table
table_iter table_begin(struct table *t){
    return t -> header -> next;
}

//Returns a pointer to the last element of the table (The footer...has no value)
table_iter table_end(struct table *t){
    return t -> footer;
}

void table_insert(struct table *t, table_iter iter, void *key, int value){
    struct table_elem *new_elem = newTableElem();
    new_elem -> key = key;
    new_elem -> value = value;
    
    new_elem -> prev = iter -> prev;
    new_elem -> next = iter;
    
    iter -> prev -> next = new_elem;
    iter -> prev = new_elem;
}

table_iter table_iter_next(table_iter iter){
    return iter -> next;
}

int table_iter_value(table_iter iter){
    assert(elem_is_internal(iter));
    return iter -> value;
}

char *table_iter_key(table_iter iter){
    assert(elem_is_internal(iter));
    return iter -> key;
}

int elem_is_internal(table_iter iter){
    return iter -> prev != NULL && iter -> next != NULL;
}

void table_insert_front(struct table *t, void *key, int value){
    table_insert(t, table_begin(t), key, value);
}

void table_insert_back(struct table *t, void *key, int value){
    table_insert(t, table_end(t), key, value);
}

void table_destroy(struct table *t){
    struct table_elem *elem = t -> header;
    while(elem != NULL){
        struct table_elem *next = elem -> next;
        freeTableElem(elem);
        elem = next;
    }
    free(t);
}

//////////////Making the list sorted///////////////

//Inserts the value along with it's key in lexicographic order
//Only inserts iff elem doesnt already have a mapping
void table_insert_next(struct table *t, void *key, int value){
    if(keyExists(t, key)){
        return;
    }
    table_iter iter = table_begin(t);
    while(elem_is_internal(iter) &&
          strcmp((char *) table_iter_key(iter), (char *) key) < 0){
        iter = table_iter_next(iter);
    }
    table_insert(t, iter, key, value);
}

//Returns 1 iff key already exists in table
int keyExists(struct table *t, char *key){
    table_iter iter = table_begin(t);
    while(elem_is_internal(iter) &&
          strcmp((char *)table_iter_key(iter), (char *) key) < 0){
        iter = table_iter_next(iter);
    }
    if(elem_is_internal(iter) &&
       strcmp((char *) table_iter_key(iter), (char *) key) == 0){
        return 1;
    } else {
        return 0;
    }
}

//Must check key exists first!
int getValue(struct table *t, char *key){
    assert(keyExists(t, key));
    table_iter iter = table_begin(t);
    while(elem_is_internal(iter) &&
          strcmp((char *)table_iter_key(iter), (char *) key) != 0){
        iter = table_iter_next(iter);
    }
    return table_iter_value(iter);
}

























