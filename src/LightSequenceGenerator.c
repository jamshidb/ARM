#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum {TIME_DELAY = 0x1D0000};

#define GENERATOR(cmpNumber, lightNum)\
 fprintf(file, "%s", "cmp r3,#");\
 fprintf(file, "%i\n", cmpNumber); \
 fprintf(file, "%s", "beq light");\
 fprintf(file, "%s", lightNum);

void prepareFile(FILE *file);
void completeFile(FILE *file);


int main(int argc, char **argv){
  if(argc == 1){
    perror("Please input the desired output file name as a command line argument\n");
    exit(EXIT_FAILURE); 
  }
  FILE *file = fopen(argv[1], "w");
  if(file == NULL){perror("Unable to create file\n"); exit(EXIT_FAILURE);}

  int count = 0;


  prepareFile(file);
  
  printf("Please enter the sequence of numbers you wish to produce and press ctrl-d once done:\n");
  char buf[10];
  int lightNumber;
  while(fgets(buf,sizeof(buf), stdin) != NULL){
   if((lightNumber = atoi(buf)) > 7){ 
      printf("Please try again, number too large\n");
    } else {
      GENERATOR(count, buf);
      count++;
    }
  }

  completeFile(file);
  fclose(file);

  char *shellCommand = malloc(200);

  strcpy(shellCommand, "./assemble ");
  
  strcat(shellCommand, argv[1]);

  strcat(shellCommand, " lightSequence.img");
  
  system(shellCommand);
  free(shellCommand);
}

void prepareFile(FILE *file){
   char prepare[] = "ldr r0,=0x20200004  ;Sets up pin 17 as output\nmov r2,#1\nlsl r2,#21\n\
str r2,[r0]\nldr r0,=0x20200008 ;sets up pin 27 as output\nmov r2,#1\nlsl r2,#21\n\
str r2,[r0]\nldr r0,=0x20200000 ;sets up pin 2 as output\nmov r2,#1\nlsl r2,#6\n\
str r2,[r0]\nmov r3,#0\n\
\nswitch:\n\nmov r7,#0x1D0000 ;Creates a delay between turning lights off and on\n\
wait1:\nsub r7,r7,#1\ncmp r7,#0x00\nbne wait1\n\n\
ldr r0,=0x20200028 ;Clears pin 2\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
\nldr r0,=0x20200028 ;Clears pin 17\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
\nldr r0,=0x20200028 ;Clears pin 27\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\n";
   fprintf(file, "%s", prepare);
}

void completeFile(FILE *file){
  char complete[] = 
"\nmov r3,#0\n\
b switch\n\n\
light0:\n\
ldr r0,=0x20200028 ;Clears pin 2\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
ldr r0,=0x20200028 ;Clears pin 17\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
ldr r0,=0x20200028 ;Clears pin 27\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light1:\n\
ldr r0,=0x2020001c ;Sets pin 17 (1)\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light2:\n\
ldr r0,=0x2020001c ;Sets pin 27 (2)\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light3:\n\
ldr r0,=0x2020001c ;Sets pin 17 (3)\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
ldr r0,=0x2020001c ;Sets pin 27 (2)\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light4:\n\
ldr r0,=0x2020001c ;Sets pin 2 (4)\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light5:\n\
ldr r0,=0x2020001c ;Sets pin 17 (5) ;Start of second half\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
ldr r0,=0x2020001c ;Sets pin 2 (4)\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light6:\n\
ldr r0,=0x2020001c ;Sets pin 27 (6)\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\
ldr r0,=0x2020001c ;Sets pin 2 (4)\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
light7:\n\
ldr r0,=0x2020001c ;Sets pin 17 (7)\n\
mov r2,#1\n\
lsl r2,#17\n\
str r2,[r0]\n\
ldr r0,=0x2020001c ;Sets pin 27 (6)\n\
mov r2,#1\n\
lsl r2,#27\n\
str r2,[r0]\n\
ldr r0,=0x2020001c ;Sets pin 2 (4)\n\
mov r2,#1\n\
lsl r2,#2\n\
str r2,[r0]\n\
\n\
add r3,r3,#1\n\
b switch\n\
\n\
andeq r0,r0,r0\n";
  fprintf(file, "%s", complete);
}
