#ifndef _emulate_h
#define _emulate_h

typedef int32_t int32;

typedef enum flag{CFLAG, ZFLAG, NFLAG}set_flag;

void read(char *fileName);
void initialise(void);
void debug(void);
int32 fetch(void);
int32 getInstructionAtLocation(int);
void decode(int32 instr, int32 *info);
int execute(int *info);

void multiply(int, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t);
uint32_t logicalShiftLeft(uint32_t registerValue, int shiftMag);
uint32_t logicalShiftRight(uint32_t registerValue, int shiftMag);
uint32_t arithmaticShiftRight(uint32_t registerValue, int shiftMag);
void testBranch(int32 *info);
void writeContent(uint32_t content, uint32_t leadingAddress);
uint32_t readContent(uint32_t leadingAddress);
void dataProcessingInfo(uint32_t instr, int *info);
void dataProcessing(int cond, int i, int opCode, int s, int Rn, int Rd, int operand2);
void setFlag(set_flag whichFlag, bool flag);
int rotateRight(int num, int bits);
void testBranch(int32 *info);
bool checkCond(uint32_t);
void printState(void);
int branch(uint32_t, int32);
void testBranch2(int32 *info);
static void enablePin(uint32_t gpioLocation);
static void togglePin(int gpioPinSet);

#endif
